#include "NtupleAnalysisBase.hxx"
#include <TVector3.h>
#include <tuple>

class AnalysisSkeleton : public NtupleAnalysisBase
{
public:
  AnalysisSkeleton(TTree *tree=0) : NtupleAnalysisBase(tree) {}
  ~AnalysisSkeleton() {}
  void Loop();
  std::tuple<int,double,TVector3> OffMuonVSL1matching(const TVector3 probePos,int skipnumber);
  double OffMuonVSHLTmatching(const TVector3 tagPos);
  bool VetoBitMatching(const int L1pt,const int roi,const int isAside,const int sectorID);
};
