#include <iostream>
#include "AnalysisSkeleton.hxx"
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <math.h>
#include <TVector.h>
#include <TFile.h>
#include <TTree.h>
#include <TVector3.h>
#include <tuple>
#include <TGraphAsymmErrors.h> 
#include <algorithm>

int Muon_roi(const double combinedEta,const double combinedPhi);

void AnalysisSkeleton::Loop()
{

  TH1D h_eta_pt6("h_eta_pt6","",100,-2.00,2.00);
  TH1D h_eta_pt4("h_eta_pt4","",100,-2.00,2.00);
  TH1D h_eta_pt4_veto("h_eta_pt4_veto","",100,-2.00,2.00);
  TH1D h_eta_pt6_sector("h_eta_pt6_sector","",100,-2.00,2.00);
  TH1D h_eta_pt4_sector("h_eta_pt4_sector","",100,-2.00,2.00);
  TH1D h_eta_pt4_sector_veto("h_eta_pt4_sector_veto","",100,-2.00,2.00);
  TH1D h_eta_pt6_nomatch("h_eta_pt6_nomatch","",100,-2.00,2.00);
  TH1D h_eta_pt4_nomatch("h_eta_pt4_nomatch","",100,-2.00,2.00);

  Long64_t nentries = fChain->GetEntriesFast();
  
  Long64_t nbytes = 0, nb = 0;
  for (Long64_t jentry=0; jentry<nentries;jentry++) {
    Long64_t ientry = LoadTree(jentry);
    if (ientry < 0) break;
    nb = fChain->GetEntry(jentry);   nbytes += nb;
    // if (Cut(ientry) < 0) continue;
    
    //    printf(" ################ event=%llu ############## \n", jentry);
    
    // Analysis code here

    bool TriggerFlag = false;

    for(int ii=0;ii<trigger_info_n;ii++){
      //      std::cout<<jentry<<" "<<trigger_info_chain->at(ii)<<std::endl;
      if(trigger_info_chain->at(ii)!="HLT_noalg_L1MU20"){continue;}
      if(trigger_info_isPassed->at(ii)==1){
	TriggerFlag = true;

      }
    }

    if(!TriggerFlag){continue;}


    for(int iMuctpi=0;iMuctpi<muctpi_nDataWords;iMuctpi++){
      const int source = muctpi_dw_source->at(iMuctpi);
      const int isForward = source -1; //RPC = -1 ,EC = 0,Forward =1
      const int L1_roi = muctpi_dw_roi->at(iMuctpi);
      const int L1_ssc = ((L1_roi+4)-(L1_roi+4)%8)/8;
      const int L1pt = muctpi_dw_thrNumber->at(iMuctpi);
      const int L1_sectorID = muctpi_dw_sectorID->at(iMuctpi);
      const int L1_isAside = muctpi_dw_hemisphere->at(iMuctpi);
      const int L1_roi_8 = (L1_roi+4)%8;      
      const int L1_Aside = (L1_isAside==1)? 0:1;
      const int veto = muctpi_dw_veto->at(iMuctpi);
   


      if(L1pt==6){
	h_eta_pt6.Fill(muctpi_dw_eta->at(iMuctpi));
      }

      if(L1pt==4){
	h_eta_pt4.Fill(muctpi_dw_eta->at(iMuctpi));
	if(veto==1){
	  h_eta_pt4_veto.Fill(muctpi_dw_eta->at(iMuctpi));
	}
      }


      if((TMath::Abs(muctpi_dw_eta->at(iMuctpi))<1.05) || (TMath::Abs(muctpi_dw_eta->at(iMuctpi))>1.92)){continue;}


      if(L1pt==6){
	h_eta_pt6_sector.Fill(muctpi_dw_eta->at(iMuctpi));
      }

      if(L1pt==4){
	h_eta_pt4_sector.Fill(muctpi_dw_eta->at(iMuctpi));
	if(veto==1){
	  h_eta_pt4_sector_veto.Fill(muctpi_dw_eta->at(iMuctpi));
	}
      }

      //      h_flow.Fill(3);




    }
  }
  
  


  //  std::cout<<deno<<" "<<nume<<" "<<nume / deno <<std::endl;   

  TFile f_TriggerModeNoiseRate("eta_distribution.root","recreate");
  h_eta_pt6.Write();
  h_eta_pt4.Write();
  h_eta_pt4_veto.Write();
  h_eta_pt6_sector.Write();
  h_eta_pt4_sector_veto.Write();
  h_eta_pt6_nomatch.Write();
  h_eta_pt4_nomatch.Write();
  f_TriggerModeNoiseRate.Write();
  
  


}

std::tuple<int,double,TVector3> AnalysisSkeleton::OffMuonVSL1matching(const TVector3 probePos,int skipnumber){

  double DeltaR_dummy = 99;
  int sector = -1;  
  TVector3 vector ;
  int number = -1;


  for(int iMu=0;iMu<mu_n;iMu++){
    //    if(iMu==skipnumber){continue;}
    const bool combinedMuon = (mu_author->at(iMu)==1 && mu_muonType->at(iMu)==0);
    if(!combinedMuon){continue;}

    const double TagEta = mu_eta->at(iMu);
    const double TagPhi = mu_phi->at(iMu);  
    //    if((TMath::Abs(mu_eta->at(iMu))>1.05)&&(TMath::Abs(mu_eta->at(iMu))<1.92)){continue;}
    TVector3 offPos;
    offPos.SetPtEtaPhi(mu_pt->at(iMu),
		       mu_eta->at(iMu),
		       mu_phi->at(iMu)
		       );



    if(offPos.DeltaR(probePos)<DeltaR_dummy){
      DeltaR_dummy = offPos.DeltaR(probePos);
      vector = offPos;
      number = iMu;
    }

  }

  return std::forward_as_tuple(number,TMath::Abs(DeltaR_dummy),vector);

//   if(TMath::Abs(DeltaR_dummy)<0.1){

//     return true;
//   }else{
//     return false;
//   }


}

double AnalysisSkeleton::OffMuonVSHLTmatching(const TVector3 tagPos){

  double DeltaR_dummy = 99;
  
  for(int ii=0;ii<trigger_info_n;ii++){
    //    if(trigger_info_isPassed->at(ii)==1){std::cout<<ii<<" "<<trigger_info_chain->at(ii)<<std::endl;}
    if(trigger_info_chain->at(ii)!="HLT_mu26_ivarmedium"){continue;}
    if(trigger_info_isPassed->at(ii)!=1){continue;}
//     std::string s = "HLT_mu";
//     std::string t = trigger_info_chain->at(ii);
//     bool isHLT = std::equal(s.begin(),s.end(),t.begin());
//     if(!isHLT){continue;}
//    if(trigger_info_typeVec->at(ii)!=0){continue;}
    const std::vector<float>& ptVec = trigger_info_ptVec->at(ii);
    const std::vector<float>& etaVec = trigger_info_etaVec->at(ii);
    const std::vector<float>& phiVec = trigger_info_phiVec->at(ii);
    const std::vector<int>& typeVec = trigger_info_typeVec->at(ii);
 
    for(int kk=0;kk<ptVec.size();kk++){

      if(typeVec.at(kk)!=0){continue;}
      TVector3 trigMuon;
      trigMuon.SetPtEtaPhi(ptVec.at(kk),
			   etaVec.at(kk),
			   phiVec.at(kk)
			   );

      //      std::cout<<kk<<" "<<trigMuon.DeltaR(tagPos)<<std::endl;

      if(trigMuon.DeltaR(tagPos)<DeltaR_dummy){

	DeltaR_dummy = trigMuon.DeltaR(tagPos);
      }
      
    }
    
  }

  return TMath::Abs(DeltaR_dummy);

}

bool AnalysisSkeleton::VetoBitMatching(const int L1_pt,const int roi,const int isAside,const int sectorID){

  std::vector<int> sl_data_index;
  std::vector<int> tile_data_index;

  bool coincidenceFlag = false;

  bool MatchingFlag = false;

  for(int iCoin=0;iCoin<TGC_coin_n;iCoin++){
    const int& type = TGC_coin_type->at(iCoin);
    const int& isStrip = TGC_coin_isStrip->at(iCoin);
    const int isInner = TGC_coin_isInner->at(iCoin);

    if(type==2){sl_data_index.push_back(iCoin);}
    if(type==1 && isInner==1 && isStrip==1){ tile_data_index.push_back(iCoin);}
  }

  for(int isl=0;isl<sl_data_index.size();isl++){
    const int& slIndex = sl_data_index.at(isl);
    const int& sl_phi = (TGC_coin_phi->at(slIndex)+1)%48;
    const int& sl_isForward = TGC_coin_isForward->at(slIndex);
    const int& sl_isAside = TGC_coin_isAside->at(slIndex);
    const int& sl_roi = TGC_coin_roi->at(slIndex);
    const int sl_ssc = ((sl_roi+4)-((sl_roi+4)%8))/8;
    const int sl_veto = TGC_coin_veto->at(slIndex);
    const int sl_roi_8 = (TGC_coin_roi->at(slIndex)+4)%8;
    const int sl_Aside = (sl_isAside==1)? 0:1;
    const int sl_pt = TGC_coin_pt->at(slIndex);
    const int& sl_bunch = TGC_coin_bunch->at(slIndex);

    if(sl_isForward!=0 || sectorID!=sl_phi || isAside!=sl_isAside || roi!=sl_roi){continue;}
    if(sl_bunch!=2){continue;}

    MatchingFlag = true;

    if(sl_veto==0){
      coincidenceFlag = true;
      break;
    }else{
      coincidenceFlag = false;
      break;
    }

  }

  //  std::cout<<MatchingFlag<<std::endl;

  return coincidenceFlag;


  
}






