#include "NtupleAnalysisBase.hxx"
#include <TVector3.h>
#include <tuple>

class AnalysisSkeleton : public NtupleAnalysisBase
{
public:
  AnalysisSkeleton(TTree *tree=0) : NtupleAnalysisBase(tree) {}
  ~AnalysisSkeleton() {}
  void Loop();
  std::tuple<bool,int> OffMuonVSL1matching(const TVector3 probePos);
  bool OffMuonVSHLTmatching(const TVector3 tagPos);
  bool VetoBitMatching(const int isAside,const int sectorID);
};
