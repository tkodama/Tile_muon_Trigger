#include <iostream>
#include "AnalysisSkeleton.hxx"
#include "EIFICoincidenceWindow.hxx"
#include "TILECoincidenceWindow.hxx"
#include <TH1D.h>
#include <TCanvas.h>
#include <math.h>
#include <TVector.h>
#include <TFile.h>
#include <TTree.h>
#include <TVector3.h>
#include <tuple>
#include <TGraphAsymmErrors.h> 

int Muon_roi(const double combinedEta,const double combinedPhi);

void AnalysisSkeleton::Loop()
{



  double deno = 0;
  double nume = 0;



  TH1D h_invariantMass("h_invariantMass","",100,0,120);
  TH1D h_efficiency_deno_A("h_efficiency_deno_A","",48,-0.5,47.5);
  TH1D h_efficiency_nume_A("h_efficiency_nume_A","",48,-0.5,47.5);
  TH1D h_efficiency_nume_veto_A("h_efficiency_nume_veto_A","",48,-0.5,47.5);
  TH1D h_efficiency_deno_C("h_efficiency_deno_C","",48,-0.5,47.5);
  TH1D h_efficiency_nume_C("h_efficiency_nume_C","",48,-0.5,47.5);
  TH1D h_efficiency_nume_veto_C("h_efficiency_nume_veto_C","",48,-0.5,47.5);
  TH1D h_efficiency_deno_pt("h_efficiency_deno_pt","",20,0,100);
  TH1D h_efficiency_nume_pt("h_efficiency_nume_pt","",20,0,100);
  TH1D h_efficiency_nume_veto_pt("h_efficiency_nume_veto_pt","",20,0,100); 
  TH1D h_efficiency_deno_eta("h_efficiency_deno_eta","",100,-2.00,2.00);
  TH1D h_efficiency_nume_eta("h_efficiency_nume_eta","",100,-2.00,2.00);
  TH1D h_L1pt("L1pt","",6,0.5,6.5);
  TH1D h_L1pt_veto("L1pt_veto","",6,0.5,6.5);
  TH1D h_count("h_count","",5,0,5);
  
  Long64_t nentries = fChain->GetEntriesFast();
  
  Long64_t nbytes = 0, nb = 0;
  for (Long64_t jentry=0; jentry<nentries;jentry++) {
    Long64_t ientry = LoadTree(jentry);
    if (ientry < 0) break;
    nb = fChain->GetEntry(jentry);   nbytes += nb;
    // if (Cut(ientry) < 0) continue;
    
    
    //    printf(" ################ event=%llu ############## \n", jentry);
    
    // Analysis code here


    for(int ii=0;ii<trig_L1_mu_n;ii++){

      //std::cout<<trig_L1_mu_thrName->at(ii)<<std::endl;
    }

    
    // Tag Muon

    int Tag_number = -1;
    double TaggedMuonPt = 0;
    double TaggedMuonEta = 0;
    double TaggedMuonPhi = 0;
    int TaggedMuonCharge = 0;

    for(int iMu=0;iMu<mu_n;iMu++){
      const bool combinedMuon = (mu_author->at(iMu)==1 && mu_muonType->at(iMu)==0);
      if(!combinedMuon){continue;}
      const double TagEta = mu_eta->at(iMu);
      const double TagPhi = mu_phi->at(iMu);  
      if((TMath::Abs(mu_eta->at(iMu))>1.05)&&(TMath::Abs(mu_eta->at(iMu))<1.92)){continue;}
      TVector3 offPos;
      offPos.SetPtEtaPhi(mu_pt->at(iMu),
			 mu_eta->at(iMu),
			 mu_phi->at(iMu)
			 );

      if(OffMuonVSHLTmatching(offPos)){
	Tag_number = iMu;
	TaggedMuonPt = mu_pt->at(iMu);
	TaggedMuonEta = mu_eta->at(iMu);
	TaggedMuonPhi = mu_phi->at(iMu);
	TaggedMuonCharge = mu_charge->at(iMu);
      }

    }       

    //    std::cout<<TaggedMuonCharge<<std::endl;
    if(Tag_number==-1){continue;}

    // probe muon


    std::vector<float> ProbePt;
    std::vector<float> ProbeEta;
    std::vector<float> ProbePhi;
    std::vector<bool> Probe_isAside;
    std::vector<double> invariantMass ;
    std::vector<TVector3> ProbePos;

    for(int iMu=0;iMu<mu_n;iMu++){
      if(iMu==Tag_number){continue;}

      const bool combinedMuon = (mu_author->at(iMu)==1 && mu_muonType->at(iMu)==0);
      if(!combinedMuon){continue;}
    


      if((TMath::Abs(mu_eta->at(iMu))<1.05)|(TMath::Abs(mu_eta->at(iMu))>1.92)){continue;}

      double deta = TMath::Abs(TaggedMuonEta - mu_eta->at(iMu));
      double dphi = TMath::Abs(TaggedMuonPhi - mu_phi->at(iMu));
      double invariantMass_dummy = sqrt(2 * TaggedMuonPt * mu_pt->at(iMu)*(cosh(deta) - cos(dphi)));
      
      
      bool isAside = 1;
      if(mu_eta->at(iMu)<0){isAside =0;}


      TVector3 probeMuon;
      probeMuon.SetPtEtaPhi(mu_pt->at(iMu),
			   mu_eta->at(iMu),
			   mu_phi->at(iMu)
			   );



      ProbePt.push_back(mu_pt->at(iMu));
      ProbeEta.push_back(mu_eta->at(iMu));
      ProbePhi.push_back(mu_phi->at(iMu));
      ProbePos.push_back(probeMuon);
      Probe_isAside.push_back(isAside);
      invariantMass.push_back(invariantMass_dummy);
    }

    //Matching Probe Muon 


    for(int ii=0;ii<ProbePt.size();ii++){
      if(invariantMass.at(ii)==0){continue;}

      //      std::cout<<"debug"<<std::endl;
      //      if(!sectorflag){continue;}
      bool probeMatchingFlag;
      int L1pt;
      std::tie(probeMatchingFlag,L1pt) = OffMuonVSL1matching(ProbePos.at(ii));
      h_invariantMass.Fill(invariantMass.at(ii)/1000);
      if(invariantMass.at(ii)/1000<80 || invariantMass.at(ii)/1000>100){continue;}
      h_efficiency_deno_pt.Fill(ProbePt.at(ii)/1000);
      h_count.Fill(0);
      if(ProbePt.at(ii)>20000){
	deno++;
	h_count.Fill(2);
	h_efficiency_deno_eta.Fill(ProbeEta.at(ii));
	if(Probe_isAside.at(ii)){    
	  h_efficiency_deno_A.Fill(Muon_roi(ProbeEta.at(ii),ProbePhi.at(ii)));
	}else{
	  h_efficiency_deno_C.Fill(Muon_roi(ProbeEta.at(ii),ProbePhi.at(ii)));
	}
      }


      if(probeMatchingFlag){
	//	std::cout<<ProbeRoI.at(ii)<<std::endl;

	h_L1pt.Fill(L1pt);
	h_efficiency_nume_pt.Fill(ProbePt.at(ii)/1000);

	if(ProbePt.at(ii)>20000){
	  //  nume++;
	  //  h_count.Fill(3);
	  h_efficiency_nume_eta.Fill(ProbeEta.at(ii));
	  if(Probe_isAside.at(ii)){    
	    h_efficiency_nume_A.Fill(Muon_roi(ProbeEta.at(ii),ProbePhi.at(ii)));
	  }else{
	    h_efficiency_nume_C.Fill(Muon_roi(ProbeEta.at(ii),ProbePhi.at(ii)));
	  }
	}    
	

	if(!VetoBitMatching(Probe_isAside.at(ii),Muon_roi(ProbeEta.at(ii),ProbePhi.at(ii)))){continue;}
	h_L1pt_veto.Fill(L1pt);
	h_count.Fill(1);
	h_efficiency_nume_veto_pt.Fill(ProbePt.at(ii)/1000);
	  
	if(ProbePt.at(ii)>20000){
	  nume++;
	  h_count.Fill(3);
	  if(Probe_isAside.at(ii)){    
	    h_efficiency_nume_veto_A.Fill(Muon_roi(ProbeEta.at(ii),ProbePhi.at(ii)));
	  }else{
	    h_efficiency_nume_veto_C.Fill(Muon_roi(ProbeEta.at(ii),ProbePhi.at(ii)));
	  }
	}    
	
      }
    }



  }

  //  std::cout<<deno<<" "<<nume<<" "<<nume / deno <<std::endl;   

  TFile f_TriggerModeEfficiency("Output_L1_efficiency.root","recreate");
  h_efficiency_deno_A.Write();
  h_efficiency_nume_A.Write();
  h_efficiency_nume_veto_A.Write();
  h_efficiency_deno_C.Write();
  h_efficiency_nume_C.Write();
  h_efficiency_nume_veto_C.Write();
  h_efficiency_deno_pt.Write();
  h_efficiency_nume_pt.Write();
  h_efficiency_nume_veto_pt.Write();
  h_efficiency_deno_eta.Write();
  h_efficiency_nume_eta.Write();
  h_L1pt.Write();
  h_L1pt_veto.Write();
  h_count.Write();
  h_invariantMass.Write();

  f_TriggerModeEfficiency.Write();






}

std::tuple<bool,int> AnalysisSkeleton::OffMuonVSL1matching(const TVector3 probePos){

  double DeltaR_dummy = 99;
  int sector = -1;  
  int pt = -1;

  for(int iMuctpi=0;iMuctpi<muctpi_nDataWords;iMuctpi++){
    const int source = muctpi_dw_source->at(iMuctpi);
    const int isForward = source -1; //RPC = -1 ,EC = 0,Forward =1
    const int L1pt = muctpi_dw_thrNumber->at(iMuctpi);
    if(isForward!=0){continue;}
    if(L1pt!=6){continue;}
    TVector3 roiPos;
    roiPos.SetPtEtaPhi(1.0,
		       muctpi_dw_eta->at(iMuctpi),
		       muctpi_dw_phi->at(iMuctpi)
		       );

    if(roiPos.DeltaR(probePos)<DeltaR_dummy){
      DeltaR_dummy = roiPos.DeltaR(probePos);
      pt = muctpi_dw_thrNumber->at(iMuctpi);
      sector = muctpi_dw_sectorID->at(iMuctpi);
    }

  }

  if(TMath::Abs(DeltaR_dummy)<0.1){

    return std::forward_as_tuple(true,pt);
  }else{
    return std::forward_as_tuple(false,-1);
  }


}

bool AnalysisSkeleton::OffMuonVSHLTmatching(const TVector3 tagPos){

  double DeltaR_dummy = 99;
  
  for(int ii=0;ii<trigger_info_n;ii++){
    if(trigger_info_chain->at(ii)!="HLT_mu26_ivarmedium"){continue;}
    if(trigger_info_isPassed->at(ii)!=1){continue;}
    const std::vector<float>& ptVec = trigger_info_ptVec->at(ii);
    const std::vector<float>& etaVec = trigger_info_etaVec->at(ii);
    const std::vector<float>& phiVec = trigger_info_phiVec->at(ii);
 
    for(int kk=0;kk<ptVec.size();kk++){
      TVector3 trigMuon;
      trigMuon.SetPtEtaPhi(ptVec.at(kk),
			   etaVec.at(kk),
			   phiVec.at(kk)
			   );

      if(trigMuon.DeltaR(tagPos)<DeltaR_dummy){
	DeltaR_dummy = trigMuon.DeltaR(tagPos);
      }
      
    }
    
  }
  if(TMath::Abs(DeltaR_dummy)<0.1){
    return true;
  }else{
    return false;
  } 
  
}

bool AnalysisSkeleton::VetoBitMatching(const int isAside,const int sectorID){

  std::vector<int> sl_data_index;
  std::vector<int> tile_data_index;

  bool tileFlag = false;
  bool coincidenceFlag = false;

  for(int iCoin=0;iCoin<TGC_coin_n;iCoin++){
    const int& type = TGC_coin_type->at(iCoin);
    const int& isStrip = TGC_coin_isStrip->at(iCoin);
    const int isInner = TGC_coin_isInner->at(iCoin);

    if(type==2){sl_data_index.push_back(iCoin);}
    if(type==1 && isInner==1 && isStrip==1){ tile_data_index.push_back(iCoin);}
  }

  for(int isl=0;isl<sl_data_index.size();isl++){
    const int& slIndex = sl_data_index.at(isl);
    const int& sl_phi = (TGC_coin_phi->at(slIndex)+1)%48;
    const int& sl_isForward = TGC_coin_isForward->at(slIndex);
    const int& sl_isAside = TGC_coin_isAside->at(slIndex);
    const int& sl_roi = TGC_coin_roi->at(slIndex);
    const int sl_ssc = ((sl_roi+4)-((sl_roi+4)%8))/8;
    const int sl_veto = TGC_coin_veto->at(slIndex);
    const int sl_roi_8 = (TGC_coin_roi->at(slIndex)+4)%8;
    const int sl_Aside = (sl_isAside==1)? 0:1;
    const int sl_pt = TGC_coin_pt->at(slIndex);
    const int& sl_bunch = TGC_coin_bunch->at(slIndex);

    if(sl_isForward!=0 | sectorID!=sl_phi | isAside!=sl_isAside){continue;}
    if(sl_bunch!=2){continue;}


    
    tileFlag = true;

    if(sl_veto==0){
      coincidenceFlag = true;
      break;
    }else{
      coincidenceFlag = false;
      break;
    }

  }

  if(tileFlag){
    if(coincidenceFlag){
      return true;
    }else{
      return false;
    }
  }else{

    return true;  

  }
  
}

int Muon_roi(const double combinedEta,const double combinedPhi){

  int Sector = -1;
  int RoI = -1;
  for(int iSector=0;iSector<24;iSector++){
    const double MinimumPhi = (M_PI / 24) * iSector;
    const double MaximumPhi = (M_PI / 24) * (iSector + 1 );
    if((MinimumPhi < TMath::Abs(combinedPhi)) && (TMath::Abs(combinedPhi) < MaximumPhi)) {
      if(combinedPhi>0){ 
	Sector = (iSector+2)%48;
      }else{
	Sector = (49 - iSector)%48;
      } 
    }

  }
  int RoI_phi = -1;
  for(int iRoI_phi=0;iRoI_phi<4;iRoI_phi++){
    double MinimumPhi = (2*M_PI /48 ) * (Sector + (iRoI_phi / 4));
    double MaximumPhi = (2*M_PI /48 ) * (Sector + ((iRoI_phi + 1) / 4));
    if((MinimumPhi < TMath::Abs(combinedPhi)) && (TMath::Abs(combinedPhi) < MaximumPhi)){
      RoI_phi = iRoI_phi;
    }

  }
  int RoI_eta = -1;
  for(int iRoI_eta = 0;iRoI_eta<37;iRoI_eta++){
    double MinimumEta = 1.05 + ((1.92 - 1.05)/37) * iRoI_eta;
    double MaximumEta = 1.05 + ((1.92 - 1.05)/37) * (iRoI_eta + 1);
    if((MinimumEta < TMath::Abs(combinedEta)) && (TMath::Abs(combinedEta) < MaximumEta)){
      RoI_eta = iRoI_eta;
    }
  }

  if((RoI_eta == -1) | (RoI_phi == -1)){
    RoI  = -1;
  }else{
    RoI = 4 * RoI_eta  + RoI_phi;
  }
  return Sector;

}





