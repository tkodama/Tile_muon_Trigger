import os



root = '6.04/14'
cmtConfig = 'x86_64-slc6-gcc49-opt'
output = 'eff.root'
inDS ='user.tkodama.data18_13TeV.00350751.physics_Main.merge.NTUP_TILEMU.1.f936_m1976.00-01-01_L1TGCNtuple/'

user = 'user.tkodama.data18_13TeV'
run = '00350751'
date = '20180524'
version = '1.1'

command = 'prun --exec "sh grid.sh %IN"'
command += ' --rootVer=%s' % root
command += ' --cmtConfig=%s' % cmtConfig
command += ' --outputs=%s' % output 
command += ' --inDS %s' % inDS
command += ' --outDS %s_%s_%s_%s' % (user, run, date ,version)

print command
os.system(command)
