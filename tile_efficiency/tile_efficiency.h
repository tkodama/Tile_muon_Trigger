//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Apr 27 15:00:41 2018 by ROOT version 6.08/06
// from TTree tree/tree
// found on file: user.tkodama.data18_13TeV_00348495_20180427_2_eff.root/user.tkodama.13861864._000001.eff.root
//////////////////////////////////////////////////////////

#ifndef tile_efficiency_h
#define tile_efficiency_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class tile_efficiency {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Double_t        deno_pt;
   Double_t        deno_phi;
   Double_t        deno_eta;
   Double_t        charge;
   Double_t        isAside;
   Double_t        ctpi_eta;
   Int_t           ctpi_ssc;
   Int_t           coincidence_flag;
   Int_t           coincidenceflag_tmdbbit_1;
   Int_t           coincidenceflag_tmdbbit_2;
   Int_t           coincidenceflag_tmdbthr_1;
   Int_t           coincidenceflag_tmdbthr_2;
   Int_t           Dlayer_flag;
   Bool_t          veto_flag;
   Bool_t          Tile_roi_flag;
   Double_t        tile_count1;
   Double_t        tile_count2;
   Int_t           sector;
   Int_t           pt20_flag;
   Double_t        DeltaR;

   // List of branches
   TBranch        *b_deno_pt;   //!
   TBranch        *b_deno_phi;   //!
   TBranch        *b_deno_eta;   //!
   TBranch        *b_charge;   //!
   TBranch        *b_isAside;   //!
   TBranch        *b_ctpi_eta;   //!
   TBranch        *b_ctpi_ssc;   //!
   TBranch        *b_coincidence_flag;   //!
   TBranch        *b_coincidenceflag_tmdbbit_1;   //!
   TBranch        *b_coincidenceflag_tmdbbit_2;   //!
   TBranch        *b_coincidenceflag_tmdbthr_1;   //!
   TBranch        *b_coincidenceflag_tmdbthr_2;   //!
   TBranch        *b_Dlayer_flag;   //!
   TBranch        *b_veto_flag;   //!
   TBranch        *b_Tile_roi_flag;   //!
   TBranch        *b_tile_count1;   //!
   TBranch        *b_tile_count2;   //!
   TBranch        *b_sector;   //!
   TBranch        *b_pt20_flag;   //!
   TBranch        *b_DeltaR;   //!

   tile_efficiency(TTree *tree=0);
   virtual ~tile_efficiency();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef tile_efficiency_cxx
tile_efficiency::tile_efficiency(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("eff.root");
      if (!f || !f->IsOpen()) {
	f = new TFile("eff.root");
      }
      f->GetObject("tree",tree);

   }
   Init(tree);
}

tile_efficiency::~tile_efficiency()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t tile_efficiency::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t tile_efficiency::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void tile_efficiency::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("deno_pt", &deno_pt, &b_deno_pt);
   fChain->SetBranchAddress("deno_phi", &deno_phi, &b_deno_phi);
   fChain->SetBranchAddress("deno_eta", &deno_eta, &b_deno_eta);
   fChain->SetBranchAddress("charge", &charge, &b_charge);
   fChain->SetBranchAddress("isAside", &isAside, &b_isAside);
   fChain->SetBranchAddress("ctpi_eta", &ctpi_eta, &b_ctpi_eta);
   fChain->SetBranchAddress("ctpi_ssc", &ctpi_ssc, &b_ctpi_ssc);
   fChain->SetBranchAddress("coincidence_flag", &coincidence_flag, &b_coincidence_flag);
   fChain->SetBranchAddress("coincidenceflag_tmdbbit_1", &coincidenceflag_tmdbbit_1, &b_coincidenceflag_tmdbbit_1);
   fChain->SetBranchAddress("coincidenceflag_tmdbbit_2", &coincidenceflag_tmdbbit_2, &b_coincidenceflag_tmdbbit_2);
   fChain->SetBranchAddress("coincidenceflag_tmdbthr_1", &coincidenceflag_tmdbthr_1, &b_coincidenceflag_tmdbthr_1);
   fChain->SetBranchAddress("coincidenceflag_tmdbthr_2", &coincidenceflag_tmdbthr_2, &b_coincidenceflag_tmdbthr_2);
   fChain->SetBranchAddress("Dlayer_flag", &Dlayer_flag, &b_Dlayer_flag);
   fChain->SetBranchAddress("veto_flag", &veto_flag, &b_veto_flag);
   fChain->SetBranchAddress("Tile_roi_flag", &Tile_roi_flag, &b_Tile_roi_flag);
   fChain->SetBranchAddress("tile_count1", &tile_count1, &b_tile_count1);
   fChain->SetBranchAddress("tile_count2", &tile_count2, &b_tile_count2);
   fChain->SetBranchAddress("sector", &sector, &b_sector);
   fChain->SetBranchAddress("pt20_flag", &pt20_flag, &b_pt20_flag);
   fChain->SetBranchAddress("DeltaR", &DeltaR, &b_DeltaR);
   Notify();
}

Bool_t tile_efficiency::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void tile_efficiency::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t tile_efficiency::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef tile_efficiency_cxx
