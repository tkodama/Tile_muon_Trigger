#include <iostream>
#include <fstream>
#include "AnalysisSkeleton.hxx"
#include "EIFICoincidenceWindow.hxx"
#include "TILECoincidenceWindow.hxx"
#include <TH1D.h>
#include <TCanvas.h>
#include <math.h>
#include <TVector.h>
#include <TFile.h>
#include <TTree.h>
#include <TVector3.h>
#include <stdlib.h>
#include <sstream>
#include <map>
#include <string>
#include <TLine.h>
#include <cstdlib>
#include <iomanip>
#include <bitset>



 
std::bitset<8> TMDB_d6(int sectorID);
std::bitset<8> TMDB_d5d6(int sectorID);
void Tile_module(int sl_sectorID,int *module1,int *module2);
void TMDB_1module(int TGC_sectorID,std::bitset<8> *module1_bit,std::bitset<8> *module2_bit);


void AnalysisSkeleton::Loop()
{

  int low_flag = 1;
  // low_thresholdの情報が欲しい時１、いらない時０



  std::string fileName = "threshold_2017_v2.txt";

  std::ifstream inputfile(fileName.c_str());
  char line[BUFSIZ];
  int counter = 0;

  int iFile=0;
  std::string name;
  int thresholdDef;
  int dummy;
  double thresholdValue;

  // registration                                                                                 
  std::map<std::pair<std::string, int>, double> thresholdMap; // (mod, thresholdDef) -> value     
  while (not (inputfile.eof())) {
    inputfile.getline(line, sizeof(line));
    //std::cout << line << std::endl;                                                             
    std::string str(line);
    if (str.empty()) { continue; }

    std::stringstream ss(line);
    ss >> name >> dummy >> thresholdDef >> thresholdValue;
    //std::cout << str << std::endl;                                                              
    //std::cout << "name=" << name << " thresholdDef=" << thresholdDef << " thresholdValue=" << t\
    //    hresholdValue << std::endl;                                                                       
    std::pair<std::string, int> key = std::make_pair(name, thresholdDef);
    thresholdMap.insert( std::make_pair(key, thresholdValue) );
  }









  double deno_pt = 0;
  double deno_phi = 0;

  double deno_eta = 0;
  // double nume_pt = 0;
  // double nume_phi_A = 0 ;
  // double nume_phi_C = 0;
  // double nume_eta =0;
  double charge = 0;
  double isAside = 0 ;
  int coincidence_flag = 0 ;
  int coincidenceflag_tmdbbit_1 = 0 ;
  int coincidenceflag_tmdbbit_2 = 0 ;
  int coincidenceflag_tmdbthr_1 = 0 ;
  int coincidenceflag_tmdbthr_2 = 0 ;
  int Dlayer_flag = -1;
  double ctpi_eta = -1;
  int ctpi_ssc = -1;
  bool veto_flag = false;
  int coin_veto = -1;
  bool Tile_roi_flag = false;
  double tile_count1 = 0;
  double tile_count2 = 0;
  double DeltaR;
  int sector = -1;
  int pt20_flag = -1;

  TFile f("eff.root","recreate");
  TTree outTree("tree","tree");
  outTree.Branch("deno_pt",&deno_pt);
  outTree.Branch("deno_phi",&deno_phi);
  outTree.Branch("deno_eta",&deno_eta);
  // outTree.Branch("nume_pt",&nume_pt);
  // outTree.Branch("nume_phi_A",&nume_phi_A);
  // outTree.Branch("nume_phi_C",&nume_phi_C);
  // outTree.Branch("nume_eta",&nume_eta);
  outTree.Branch("charge",&charge);
  outTree.Branch("isAside",&isAside);
  outTree.Branch("ctpi_eta",&ctpi_eta);
  outTree.Branch("ctpi_ssc",&ctpi_ssc);
  outTree.Branch("coincidence_flag",&coincidence_flag);
  outTree.Branch("coincidenceflag_tmdbbit_1",&coincidenceflag_tmdbbit_1);
  outTree.Branch("coincidenceflag_tmdbbit_2",&coincidenceflag_tmdbbit_2);
  outTree.Branch("coincidenceflag_tmdbthr_1",&coincidenceflag_tmdbthr_1);
  outTree.Branch("coincidenceflag_tmdbthr_2",&coincidenceflag_tmdbthr_2);
  outTree.Branch("Dlayer_flag",&Dlayer_flag);
  outTree.Branch("veto_flag",&veto_flag);
  outTree.Branch("coin_veto",&coin_veto);
  outTree.Branch("Tile_roi_flag",&Tile_roi_flag);
  outTree.Branch("tile_count1",&tile_count1);
  outTree.Branch("tile_count2",&tile_count2);
  outTree.Branch("sector",&sector);
  outTree.Branch("pt20_flag",&pt20_flag); 
  outTree.Branch("DeltaR",&DeltaR);  

  if (fChain == 0) return;

  //  printf("################START#################");
  


  std::vector<double> deno_A(48,0);
  std::vector<double> nume_A(48,0);
  std::vector<double> deno_C(48,0);
  std::vector<double> nume_C(48,0);
  

  
  Long64_t nentries = fChain->GetEntriesFast();
  
  Long64_t nbytes = 0, nb = 0;
  for (Long64_t jentry=0; jentry<nentries;jentry++) {
    Long64_t ientry = LoadTree(jentry);

    if (ientry < 0) break;
    nb = fChain->GetEntry(jentry);   nbytes += nb;
    // if (Cut(ientry) < 0) continue;
    
    //    if(jentry%1000==0){    
    //      printf(" ################ event=%llu bcid=%d ############## \n", jentry,bcid);
      //   }
    // Analysis code here
   
    

    

    
    std::vector<int> sl_data_index;
    std::vector<int> tile_data_index;

    
    for (int iCoin=0; iCoin<TGC_coin_n; iCoin++) {
      const int& type = TGC_coin_type->at(iCoin);
      const int& isStrip = TGC_coin_isStrip->at(iCoin);
      const int& isInner = TGC_coin_isInner->at(iCoin);
      
      
      if (type==2) { sl_data_index.push_back(iCoin); }
      if (type==1 && isInner==1 && isStrip==1) { tile_data_index.push_back(iCoin); 
	//	std::cout<<TGC_coin_pt->at(iCoin)<<std::endl;
      }
    }
    
    
  
    int tmp1=99;
    int tmp2=0;
    


    for(int ii=0;ii<trigger_info_n;ii++){
      std::vector <int> typeVec = trigger_info_typeVec->at(ii);
      std::vector <float> ptVec = trigger_info_ptVec->at(ii);
      std::vector <float> etaVec = trigger_info_etaVec->at(ii);
      std::vector <float> phiVec = trigger_info_phiVec->at(ii);
      
      
      for(int jj=0;jj<trigger_info_nTracks->at(ii);jj++){
	

	if(tmp1==0){continue;}
	tmp1=typeVec.at(jj);
      }
    }
    
    for(int iMu=0;iMu<mu_n;iMu++){
      if(tmp1==0&&mu_author->at(iMu)==1&&mu_muonType->at(iMu)==0){
	

	if(std::abs(mu_eta->at(iMu))>1.0){
	  tmp2=1;
	}
      }
    }
    
    
    
    int tmp3=99999;
    
    for(int iMuctpi=0;iMuctpi<muctpi_nDataWords;iMuctpi++){
      const int L1_isForward = muctpi_dw_source->at(iMuctpi);
      const int L1_roi = muctpi_dw_roi->at(iMuctpi);
      const int L1_ssc = ((L1_roi+4)-(L1_roi+4)%8)/8;
      const int L1_isAside = muctpi_dw_hemisphere->at(iMuctpi);
      const bool L1_roi_isTileRegion = (L1_ssc < 6) && ( L1_isForward == 1); 
      const int L1_sectorID = muctpi_dw_sectorID->at(iMuctpi);
      const int L1_charge = muctpi_dw_charge->at(iMuctpi);      
      const int L1_roi_8 = (L1_roi+4)%8;      
      const int L1_Aside = (L1_isAside==1)? 0:1;
      const int L1_pt = muctpi_dw_thrNumber->at(iMuctpi);
      const double L1_eta = muctpi_dw_eta->at(iMuctpi);
      const int L1_veto = muctpi_dw_veto->at(iMuctpi);

      ctpi_eta = L1_eta;
      ctpi_ssc = L1_ssc;
      isAside = L1_isAside;

      if(!L1_roi_isTileRegion){continue;}




	if(L1_isAside==1){
	  if(L1_sectorID==0|L1_sectorID==1|L1_sectorID==2|L1_sectorID==3|L1_sectorID==6|L1_sectorID==7|L1_sectorID==8|L1_sectorID==9|L1_sectorID==14|L1_sectorID==15|L1_sectorID==19|L1_sectorID==20|L1_sectorID==21|L1_sectorID==26|L1_sectorID==27|L1_sectorID==30|L1_sectorID==31|L1_sectorID==32|L1_sectorID==36|L1_sectorID==37|L1_sectorID==38||L1_sectorID==39|L1_sectorID==43|L1_sectorID==44|L1_sectorID==45){
	    if(L1_ssc!=3){continue;}
	  }else{
	    if(L1_ssc>4){continue;}
	  }
	  
	}else{
	  if(L1_sectorID==0|L1_sectorID==1|L1_sectorID==2|L1_sectorID==3|L1_sectorID==6|L1_sectorID==7|L1_sectorID==8|L1_sectorID==9|L1_sectorID==12|L1_sectorID==15|L1_sectorID==19|L1_sectorID==20|L1_sectorID==21|L1_sectorID==24|L1_sectorID==25|L1_sectorID==26|L1_sectorID==27|L1_sectorID==30|L1_sectorID==31|L1_sectorID==32|L1_sectorID==36|L1_sectorID==37|L1_sectorID==38|L1_sectorID==39|L1_sectorID==43|L1_sectorID==44|L1_sectorID==45){
	    if(L1_ssc!=3){continue;}
	  }else{
	    if(L1_ssc>4){continue;}
	  }
	}	    

      if((EIFI_FlagRoI[L1_sectorID][L1_ssc][7-L1_roi_8]==0)&&(TILE_FlagRoI[L1_Aside][L1_sectorID][L1_ssc][7-L1_roi_8]==1)&&(L1_pt==6)){
	Tile_roi_flag = true;
      }else{Tile_roi_flag = false;}   //tmp_change

      if(L1_veto==1){
	veto_flag = true;
      }else{veto_flag = false;}	
	

	int D_layer_flag =1;
	//D5+D6->D_layer_flag = 1,D6->D_layer_flag = 0;
	int Map_H=4;
	int Map_L=4;
	
	if(L1_ssc==5){D_layer_flag = 0;}
	
	if(D_layer_flag ==1){
	  Map_H=3;
	  Map_L=2;
	}else{
	  Map_H=1;
	  Map_L=0;
	}
	Dlayer_flag = D_layer_flag;


	if(muctpi_dw_source->at(iMuctpi)!=1){continue;}
	

	double dummy_deltaR = 999;
	
	for(int ii=0;ii<mu_n;ii++){
	  
	  float ptVec = mu_pt->at(ii);
	  float etaVec = mu_eta->at(ii);
	  float phiVec = mu_phi->at(ii);
	  int offline_mu_charge = mu_charge->at(ii);  

	  if((mu_author->at(ii)!=1)|(mu_muonType->at(ii)!=0)|(std::abs(mu_eta->at(ii))<1.0)){continue;}
	  if(ptVec<20000){continue;}
	  
	  charge = offline_mu_charge;
	  
	  TVector3 v1 (0,0,0);
	  v1.SetPtEtaPhi(ptVec,etaVec,phiVec);
	  TVector3 v2 (0,0,0);
	  v2.SetPtEtaPhi(ptVec,muctpi_dw_eta->at(iMuctpi),muctpi_dw_phi->at(iMuctpi));
	  
	  double deltaR = v1.DeltaR(v2);
	  
	  if(dummy_deltaR>deltaR){
	    dummy_deltaR = deltaR;
	    deno_pt = ptVec;
	    deno_eta = etaVec;
	    deno_phi = L1_sectorID;
	  }
	  
	  
	  
	  
	  

	  
	  //	  if(tmp3==iMuctpi){break;}
	  //	  if(deltaR<0.1){break;}
	    
	  tmp3=iMuctpi;
	  	  
	  if(L1_isAside==1){
	    deno_A.at(L1_sectorID) += 1;
	  }else{
	    deno_C.at(L1_sectorID) += 1;
	  }
	  
	  
	  
	}

	DeltaR = dummy_deltaR;	  
	sector = L1_sectorID;  


	if(deno_pt>20000){
	  pt20_flag = 1;
	}else{
	  pt20_flag = 0;
	}
	

	for(int iCoin =0;iCoin<TGC_coin_n;iCoin++){

	  const int coin_type = TGC_coin_type->at(iCoin);
	  const int coin_isAside = TGC_coin_isAside->at(iCoin);
	  const int coin_isInner = TGC_coin_isInner->at(iCoin);
	  const int coin_isStrip = TGC_coin_isStrip->at(iCoin);
	  const int coin_phi = (TGC_coin_phi->at(iCoin)+1)%48;
	  const int coin_bunch = TGC_coin_bunch->at(iCoin);
	  const int coin_isForward = TGC_coin_isForward->at(iCoin);
	  //	  const int coin_veto = TGC_coin_veto->at(iCoin);
	  const std::string side = (coin_isAside) ? "A" : "C" ;
	      
	  
	  //		std::cout<<coin_veto<<std::endl;
	  
	  if(coin_isForward!=0){continue;}
	  if(coin_bunch!=2){continue;}
	  
	  if(coin_type!=1){continue;}
	  if(coin_isInner!=1){continue;}
	  if(coin_isStrip!=1){continue;}
	  if(coin_phi!=L1_sectorID){continue;}
	  if(coin_isAside!=L1_isAside){continue;}	      
	  
	  //      std::cout<<L1_isAside<<" "<<L1_sectorID<<" "<<L1_roi<<" "<<iMuctpi<<std::endl;	  	      
	      
	  std::bitset<8> TMDB_bit = 0b00000000;

	  //	      TMDB_bit = TMDB_d6(coin_phi) | TMDB_d5d6(coin_phi);
	  if(L1_ssc==5){
	    TMDB_bit = TMDB_d6(coin_phi);      		    
	  }else{
	    TMDB_bit= TMDB_d5d6(coin_phi);
	  }
	  std::bitset<8> coin_bit =	TGC_coin_inner->at(iCoin);
	  std::bitset<8> sum = coin_bit&TMDB_bit;      		    
	  
	  coin_veto = TGC_coin_veto->at(iCoin);	    
	      
	      
	  if(sum==0b00000000){
	    coincidence_flag = 0 ;	
	  }
	  if(sum!=0b00000000){
	    coincidence_flag = 1;
	  }
	}  


	double tile_energy = 0;
	double tile_count = 0;
	int tmp_isA=99;
	int tmp_ID=99;	
	int TGC_coin_sector = 99;
	
	int module1=0;
	int module2=0;
	
	      
	    
	Tile_module(L1_sectorID,&module1,&module2);		    
	      
	    
	
	for(int iTile = 0;iTile<TILE_murcv_raw_n;iTile++){
	  int moduleID = TILE_murcv_raw_drawer->at(iTile);
	  
	  int Tile_isAside = (TILE_murcv_raw_ros->at(iTile)==3) ? 1 : 0 ;
	  //	      int Tile_count = TILE_murcv_raw_count->at(iTile);  
	  
	  if(Tile_isAside!=L1_isAside){continue;}
	  if(moduleID!=module1&&moduleID!=module2){continue;}
	  
	  int Trig_bit1=0;		  
	  int Trig_bit2=0;		  
	    
	      
	  std::ostringstream oss;
	    
	  oss << "EBA" << std::setfill('0')<< std::setw(2) << moduleID +1 ;
	  std::string str = oss.str(); 
	  
	  double xValueH = thresholdMap.at( std::make_pair(str,Map_H)); 
	  double xValueL = thresholdMap.at( std::make_pair(str,Map_L)); 
	    
	  std::ostringstream oss1;
	      
	  oss1 << "EBC" << std::setfill('0')<< std::setw(2) << moduleID +1 ;
	  std::string str1 = oss1.str(); 
	  
	  double xValueH_C = thresholdMap.at( std::make_pair(str1,Map_H)); 
	  double xValueL_C = thresholdMap.at( std::make_pair(str1,Map_L)); 
	  
	  double thrH = 0 ;
	  double thrL = 0 ;
	  if(D_layer_flag==1){
	    if(Tile_isAside==1){
	      thrH = xValueH;
	      thrL = xValueL;
	    }else{
	      thrH = xValueH_C;
	      thrL = xValueL_C;
	    }
	  }else{
	    if(Tile_isAside==1){
	      thrH = xValueH;
	      thrL = xValueL;
	      
	    }else{
	      thrH = xValueH_C;
	      thrL = xValueL_C;
	    }
	  }
	    
	  if(D_layer_flag==1){		 
	    if(TILE_murcv_raw_channel->at(iTile)==0){
	      tmp_isA=Tile_isAside;
	      tmp_ID = moduleID;
	      tile_energy = TILE_murcv_raw_energy->at(iTile);
	      tile_count = TILE_murcv_raw_count->at(iTile);
	      //		    std::cout<<TILE_murcv_raw_count->at(iTile)<<std::endl;
	    }else if((tmp_isA==Tile_isAside)&&(tmp_ID==moduleID)&&(TILE_murcv_raw_channel->at(iTile)>0)){
	      tile_energy += TILE_murcv_raw_energy->at(iTile);
	      tile_count += TILE_murcv_raw_count->at(iTile);
	    }
	  }else{
	    if(TILE_murcv_raw_channel->at(iTile)==2){
	      tmp_isA=Tile_isAside;
	      tmp_ID = moduleID;
	      tile_energy = TILE_murcv_raw_energy->at(iTile);
	      tile_count = TILE_murcv_raw_count->at(iTile);
	      //		    std::cout<<TILE_murcv_raw_count->at(iTile)<<std::endl;
	    }else if((tmp_isA==Tile_isAside)&&(tmp_ID==moduleID)&&(TILE_murcv_raw_channel->at(iTile)==3)){
	      tile_energy += TILE_murcv_raw_energy->at(iTile);
	      tile_count += TILE_murcv_raw_count->at(iTile);
	    }
	  }
	  
	  if(TILE_murcv_raw_channel->at(iTile)!=3){continue;}
	  
	  for(int iTile_trig=0;iTile_trig<TILE_murcv_trig_n;iTile_trig++){
	    int trig_mod = TILE_murcv_trig_mod->at(iTile_trig);
	    int Trig_isAside = (TILE_murcv_trig_part->at(iTile_trig)==3) ? 1 : 0 ;
	    if(trig_mod!=moduleID|Trig_isAside!=Tile_isAside){continue;}
	    
	    if(D_layer_flag==1){
	      Trig_bit1 =TILE_murcv_trig_bit0->at(iTile_trig)+TILE_murcv_trig_bit1->at(iTile_trig);
	      Trig_bit2 =TILE_murcv_trig_bit0->at(iTile_trig);
	    }else{
	      Trig_bit1 =TILE_murcv_trig_bit2->at(iTile_trig)+TILE_murcv_trig_bit3->at(iTile_trig);
	      Trig_bit2 =TILE_murcv_trig_bit2->at(iTile_trig);
	    }
	    
	    
	  }
	      


	      
	      
	  if(moduleID==module1){
	    coincidenceflag_tmdbbit_1 = (Trig_bit1==0)? 0 : 1 ;
	    coincidenceflag_tmdbthr_1 = (tile_count>thrL)? 1 : 0 ; 
	    tile_count1 = tile_count;
	    
	      
	    if(coincidenceflag_tmdbthr_1==0&&coincidenceflag_tmdbthr_2==0){
	      //		  std::cout<<L1_isAside<<" "<<L1_sectorID<<" "<<thrL - tile_energy<<std::endl;
	    }
	    
	  }
	  if(moduleID==module2){
	    coincidenceflag_tmdbbit_2 = (Trig_bit1==0)? 0 : 1 ;
	    coincidenceflag_tmdbthr_2 = (tile_count>thrL)? 1 : 0;
	    tile_count2 = tile_count;
	    if(coincidenceflag_tmdbthr_2==0){
	      //		  std::cout<<L1_isAside<<" "<<L1_sectorID<<" "<<thrL - tile_energy<<std::endl;
	    }
	    
	  }
	  
	
	}
	outTree.Fill();	     	        	    
      
    }
  }
  
  

  outTree.Write();
  f.Write();


}
std::bitset<8> TMDB_d5d6(int sectorID){
  
  std::bitset<8> module=0b00000000 ;
  if(sectorID>1){
    switch((sectorID)%6)
      {
      case 2:
	module = 0b00010100;
	break;
      case 3:
	module = 0b01010000;
	break;
      case 4:
	module = 0b00000101;
	break;
      case 5:
	module = 0b01010000;
	break;
      case 0:
	module = 0b00000101;
	break;
      case 1:
	module = 0b00010100;
	break;
      }
  }else{
    switch(sectorID)
      {
      case 0:
	module = 0b00000101;
	break;
      case 1:
	module = 0b00010100;
	break;
      }
  }
  
  return module;
  
  
}

std::bitset<8> TMDB_d6(int sectorID){
  
  std::bitset<8> module=0b00000000 ;
  if(sectorID>1){
    switch((sectorID)%6)
      {
      case 2:
	module = 0b00101000;
	break;
      case 3:
	module = 0b10100000;
	break;
      case 4:
	module = 0b00001010;
	break;
      case 5:
	module = 0b10100000;
	break;
      case 0:
	module = 0b00001010;
	break;
      case 1:
	module = 0b00101000;
	break;
      }
  }else{
    switch(sectorID)
      {
      case 0:
	module = 0b00001010;
	break;
      case 1:
	module = 0b00101000;
	break;
      }
  }
  
  return module;
  
  
}


void Tile_module(int sl_sectorID,int *module1,int *module2){
  
   
   

  for(int nn=0;nn<8;nn++){   
    if(sl_sectorID>1){
      if(sl_sectorID==6*nn+2){
	*module1=8*nn;
	*module2=8*nn+1;
      }else if(sl_sectorID==6*nn+3){
	*module1=8*nn+1;
	*module2=8*nn+2;
      }else if(sl_sectorID==6*nn+4){
	*module1=8*nn+2;
	*module2=8*nn+3;
      }else if(sl_sectorID==6*nn+5){
	*module1=8*nn+4;
	*module2=8*nn+5;
      }else if(sl_sectorID==6*nn+6){
	*module1=8*nn+5;
	*module2=8*nn+6;
      }else if(sl_sectorID==6*nn+7){
	*module1=8*nn+6;
	*module2=8*nn+7;
      }
    }
    if(sl_sectorID==0){
	*module1=61;
	*module2=62;
    }
    if(sl_sectorID==1){
	*module1=62;
	*module2=63;
    }    

  }
  
}
 
void TMDB_1module(int TGC_sectorID,std::bitset<8> *module1_bit,std::bitset<8> *module2_bit){

  if(TGC_sectorID>1){
    switch((TGC_sectorID)%6)
      {
      case 2:
	*module1_bit = 0b00001100;
	*module2_bit = 0b00110000;
	break;
      case 3:
	*module1_bit = 0b00110000;
	*module2_bit = 0b11000000;
	break;
      case 4:
	*module1_bit = 0b00000011;
	*module2_bit = 0b00001100;
	break;
      case 5:
	*module1_bit = 0b00110000;
	*module2_bit = 0b11000000;

	break;
      case 0:
	*module1_bit = 0b00000011;
	*module2_bit = 0b00001100;
	
	break;
      case 1:
	*module1_bit = 0b00001100;
	*module2_bit = 0b00110000;
	break;
      }
  }else{
    switch(TGC_sectorID)
      {
      case 0:
	*module1_bit = 0b00000011;
	*module2_bit = 0b00001100;
	break;
      case 1:
	*module1_bit = 0b00001100;
	*module2_bit = 0b00110000;
	break;
      }
  }
  
}

